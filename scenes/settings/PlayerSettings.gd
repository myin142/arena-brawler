extends Popup

var opened = false

func _ready():
	$MenuContainer/Character.grab_focus()

func _input(event):
	if event.is_action_pressed("player_settings"):
		_toggle_player_settings()
		
func _toggle_player_settings():
	if visible:
		hide()
	else:
		popup_centered(Vector2(80, 100))
