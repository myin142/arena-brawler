extends Spatial

class_name HitArea

export var damage = 1
export var damage_rate_in_seconds: float = 0

var _entered_nodes = []
var _elapsed_time = 0

# TODO: seperate elapsed time for each node
func _process(delta: float) -> void:
	_elapsed_time += delta
	if damage_rate_in_seconds != 0 and _elapsed_time >= damage_rate_in_seconds:
		_elapsed_time = 0
		for node in _entered_nodes:
			node.damage(damage)

func _on_Area_body_entered(body: Node) -> void:
	if _is_damageable_node(body):
		body.damage(damage)
		if damage_rate_in_seconds > 0:
			_entered_nodes.append(body)

func _on_Area_body_exited(body: Node) -> void:
	if _is_damageable_node(body) and _entered_nodes.has(body):
		var idx = _entered_nodes.find(body)
		_entered_nodes.remove(idx)

func _is_damageable_node(node: Node) -> bool:
	return node.has_method("damage")
