extends State

class_name Move

export var move_speed = 300

export var air_state_path: NodePath
onready var air_state: State = get_node(air_state_path)

# Ray cast to help detect if character is on floor
# Improves floor detection on uneven surfaces
export var ground_cast_path: NodePath
onready var ground_cast: RayCast = get_node(ground_cast_path)

# Body to rotate
export var body_path: NodePath
onready var body = get_node(body_path)

# Character to move
export var character_path: NodePath
onready var character: KinematicBody = get_node(character_path)

onready var gravity = ProjectSettings.get_setting("physics/3d/default_gravity") * ProjectSettings.get_setting("physics/3d/default_gravity_vector")

var motion = Vector2()
var velocity = Vector3()

func _ready():
	if character == null and owner is KinematicBody:
		character = owner

func _get_configuration_warning() -> String:
	if body and character:
		return ""
	
	return "Missing body or character"

func physics_process(delta: float) -> void:
	if motion.length() > 1:
		motion = motion.normalized()

	velocity.x = motion.x * move_speed * delta
	velocity.z = motion.y * move_speed * delta

	var direction = Vector3(velocity.x, 0, velocity.z) * -1
	if direction.length() > 0.01:
		var look_target = body.global_transform.origin + direction
		body.look_at(look_target, Vector3.UP)

	velocity += gravity * delta
	velocity = character.move_and_slide(velocity, Vector3.UP, true, 4, deg2rad(45))
	
	if not _is_grounded() and air_state: 
		_state_machine.transition_to(air_state)

func _is_grounded() -> bool:
	var grounded = ground_cast and ground_cast.is_colliding()
	return character.is_on_floor() or grounded
