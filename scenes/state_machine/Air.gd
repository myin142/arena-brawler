extends State

class_name Air

signal air

export var move_state_path: NodePath
onready var move_state: State = get_node(move_state_path)

func _get_configuration_warning() -> String:
	return "Move State required" if move_state == null else ""

func physics_process(delta: float) -> void:
	_parent.physics_process(delta)
	if _parent.character.is_on_floor():
		_state_machine.transition_to(move_state)
		
func enter() -> void:
	emit_signal("air")
