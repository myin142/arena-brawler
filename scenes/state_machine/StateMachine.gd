extends Node
class_name StateMachine
# Generic State Machine. Initializes states and delegates engine callbacks
# (_physics_process, _unhandled_input) to the active state.

export var initial_state := NodePath()

onready var state: State = get_node(initial_state)

func _get_configuration_warning():
	return "Initial state is required" if state == null else ""

func _init() -> void:
	add_to_group("state_machine")

func _ready() -> void:
	state.enter()

func _unhandled_input(event: InputEvent) -> void:
	state.unhandled_input(event)

func _process(delta: float) -> void:
	state.process(delta)

func _physics_process(delta: float) -> void:
	state.physics_process(delta)

func transition_to(target_state: State) -> void:
	state.exit()
	state = target_state
	state.enter()
