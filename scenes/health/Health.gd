extends Node

# Responsible for keeping track of the health and preventing invalid values
class_name Health

signal zero_health
signal changed(hp)
signal max_changed(hp)

export var max_health = 10 setget set_max_health
onready var health = max_health setget set_health

func set_health(hp: int) -> void:
	health = clamp(hp, 0, max_health)
	emit_signal("changed", health)
	
	if health == 0:
		emit_signal("zero_health")
	
func set_max_health(hp: int) -> void:
	var prev_max_health = max_health
	max_health = hp if hp >= 1 else 1

	if prev_max_health < max_health:
		self.health += max_health - prev_max_health

	emit_signal("max_changed", max_health)
