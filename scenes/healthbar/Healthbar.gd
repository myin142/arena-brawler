extends Spatial

class_name Healthbar

export var health_path: NodePath

onready var health: Health = get_node(health_path)
onready var hp_bar: ProgressBar = $Sprite3D/Viewport/ProgressBar

func _ready():
	health.connect("changed", self, "set_health")
	health.connect("max_changed", self, "set_max_health")

func set_health(hp: int) -> void:
	hp_bar.set_value(hp)

func set_max_health(hp: int) -> void:
	hp_bar.set_max(hp)
