extends Node

# Manages the stats of a character and calculates values based on the stats
class_name CharacterStats

export(int) var strength = 5
export(int) var vitality = 5
export(int) var agility = 5
export(int) var dexterity = 5
export(int) var intelligence = 5

func get_max_health() -> int:
	return vitality * 10
