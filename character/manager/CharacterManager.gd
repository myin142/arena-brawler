extends Node

class_name CharacterManager

onready var stats: CharacterStats = $CharacterStats
onready var health: Health = $Health

func _ready():
	health.max_health = stats.get_max_health()
	health.health = health.max_health

func damage(dmg: int) -> void:
	health.health -= dmg
	pass
