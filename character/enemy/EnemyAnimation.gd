extends AnimationTree

class_name EnemyAnimation

const MOVEMENT_BLEND = "parameters/move/blend_amount"
const CURRENT_STATE = "parameters/state/current"
const ATTACK = "parameters/attack/active"

const GROUND_STATE = 0
const AIR_STATE = 1

func move(move_direction: Vector2) -> void:
	var animation: AnimationPlayer = get_node(anim_player)
	if move_direction.length() > 0.01:
		animation.current_animation = "walk"
	else:
		animation.current_animation = "idle"
