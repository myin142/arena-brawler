extends State

class_name EnemyMove

signal move(direction)

var last_player: Node
var last_player_position: Vector3

func physics_process(delta: float) -> void:
	_update_motion()
	_parent.physics_process(delta)
	
	emit_signal("move", _parent.motion)

func _update_motion():
	var motion

	if last_player == null:
		if last_player_position == null or last_player_position.length() <= 0.5:
			motion = Vector2.ZERO
		else:
			motion = _create_motion_for_target(last_player_position)
	else:
		motion = _create_motion_for_target(last_player.global_transform.origin)

	if motion.length() <= 0.5:
		motion = Vector2.ZERO
	else:
		motion = motion.normalized()
	
	_parent.motion = motion

func _create_motion_for_target(target: Vector3) -> Vector2:
	if _parent.character.global_transform.origin.distance_to(target) <= 0.5:
		return Vector2.ZERO

	var dir = _parent.character.global_transform.origin.direction_to(target)	
	return Vector2(dir.x, dir.z)

func set_player(body: Node) -> void:
	last_player = body

func player_exit(body: Node) -> void:
	if body == last_player:
		last_player_position = last_player.global_transform.origin
		last_player = null
