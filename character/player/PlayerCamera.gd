extends Spatial

class_name PlayerCamera

const MOUSE_SENSITIVITY = 0.1
const JOYSTICK_SENSITIVITY = 3

onready var camera_rot = $CameraRot

var verticalJoy: float = 0
var horizontalJoy: float = 0

func _init():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
	# On idle it might still output a small value
	if abs(verticalJoy) > 0.1 or abs(horizontalJoy) > 0.1:
		_rotate_camera_x(verticalJoy * JOYSTICK_SENSITIVITY)
		_rotate_camera_y(horizontalJoy * JOYSTICK_SENSITIVITY)

func _unhandled_input(event: InputEvent) -> void:
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return

	if event is InputEventMouseMotion:
		_rotate_camera_x(event.relative.y * MOUSE_SENSITIVITY)
		_rotate_camera_y(event.relative.x * MOUSE_SENSITIVITY)
	
	if event is InputEventJoypadMotion:
		verticalJoy = Input.get_joy_axis(0, JOY_AXIS_3)
		horizontalJoy = Input.get_joy_axis(0, JOY_AXIS_2)
	
func _rotate_camera_x(value):
	camera_rot.rotate_x(deg2rad(value))

	var rot = camera_rot.rotation_degrees
	rot.x = clamp(rot.x, -70, 70)
	camera_rot.rotation_degrees = rot

func _rotate_camera_y(value):
	self.rotate_y(deg2rad(value * -1))

func target_direction_for_motion(motion) -> Vector3:
	var camera_basis = camera_rot.global_transform.basis
	var camera_x = _normalized_basis(camera_basis.x)
	var camera_z = _normalized_basis(camera_basis.z)
	
	return camera_x * motion.x + camera_z * motion.y

func _normalized_basis(basis) -> Vector3:
	basis.y = 0
	return basis.normalized()
