extends AnimationTree

class_name PlayerAnimation

const MOVEMENT_BLEND = "parameters/move/blend_amount"
const CURRENT_STATE = "parameters/state/current"
const ATTACK = "parameters/attack/active"

const GROUND_STATE = 0
const AIR_STATE = 1

func move(move_direction: Vector2) -> void:
	set(CURRENT_STATE, GROUND_STATE)
	set(MOVEMENT_BLEND, move_direction.length())

func air() -> void:
	set(CURRENT_STATE, AIR_STATE)

func attack():
	set(ATTACK, true)
