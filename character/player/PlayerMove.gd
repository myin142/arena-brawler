extends State

class_name PlayerMove

signal move(direction)

export var jump_force = 5

export var camera_path: NodePath
onready var camera: PlayerCamera = get_node(camera_path)

var side_strength = 0
var forward_strength = 0

func _get_configuration_warning() -> String:
	return "Missing camera" if camera == null else ""

func unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("jump"):
		_parent.velocity.y = jump_force
	elif event.is_action("move_left") or event.is_action("move_right") or event.is_action("move_forward") or event.is_action("move_back"):
		_update_input()

func physics_process(delta: float) -> void:
	var motion = Vector2(side_strength, forward_strength)
	var target = camera.target_direction_for_motion(motion)

	var target_2d = Vector2(target.x, target.z)
	_parent.motion = motion.rotated(motion.angle_to(target_2d))
	_parent.physics_process(delta)
	
	emit_signal("move", motion)
#
func enter(msg := {}) -> void:
	_update_input()

func _update_input() -> void:
	side_strength = Input.get_action_strength("move_left") - Input.get_action_strength("move_right")
	forward_strength = Input.get_action_strength("move_forward") - Input.get_action_strength("move_back")
