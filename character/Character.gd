extends KinematicBody

class_name Character

export var character_manager_path: NodePath
onready var character_manager: CharacterManager = get_node(character_manager_path)

func _get_configuration_warning() -> String:
	return "Missing character manager" if character_manager == null else ""

func damage(dmg: int) -> void:
	character_manager.damage(dmg)
