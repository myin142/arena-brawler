extends Spatial

var bullet_scene = preload("Bullet.tscn")

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("attack"):
		fire()

func fire() -> void:
	var clone = bullet_scene.instance()
	add_child(clone)
