extends Spatial

class_name Bullet

export var speed = 100

const KILL_TIMER = 2
var timer = 0

func _physics_process(delta: float) -> void:
	var dir = global_transform.basis.x.normalized()
	global_translate(dir * speed * delta)

	timer += delta
	if timer >= KILL_TIMER:
		kill(null)

func kill(body: Node) -> void:
	queue_free()
