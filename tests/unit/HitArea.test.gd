extends WAT.Test

var hit_area: HitArea
var damage = 1
var node: Mockito.ScriptDirector

func pre():
	hit_area = HitArea.new()
	hit_area.damage = damage
	hit_area.damage_rate_in_seconds = 0
	
	node = Mockito.mock(Character)
	node.method("damage")

func test_apply_damage_once():
	hit_area._on_Area_body_entered(node.double())
	simulate(hit_area, 1, 1)

	asserts.is_equal(node.call_count("damage"), 1)
	asserts.was_called_with_arguments(node, "damage", [damage])
	
func test_apply_damage_in_rates():
	hit_area.damage_rate_in_seconds = 1
	hit_area._on_Area_body_entered(node.double())
	simulate(hit_area, 2, 0.5)
	
	asserts.is_equal(node.call_count("damage"), 2)
	asserts.was_called_with_arguments(node, "damage", [damage])

func test_stop_apply_damage_in_rates_after_exit():
	hit_area.damage_rate_in_seconds = 1
	var double = node.double()
	hit_area._on_Area_body_entered(double)
	simulate(hit_area, 1, 1)
	
	hit_area._on_Area_body_exited(double)
	simulate(hit_area, 1, 1)
	
	asserts.is_equal(node.call_count("damage"), 2)
	asserts.was_called_with_arguments(node, "damage", [damage])

