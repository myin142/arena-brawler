extends WAT.Test

var state_machine: StateMachine
var state: Mockito.ScriptDirector

func pre():
	state_machine = StateMachine.new()

	state = Mockito.mock(State)
	state.method("enter")
	state.method("exit")
	state.method("process")
	state.method("physics_process")
	state.method("unhandled_input")
	
	Mockito.add_child(state_machine, state, "State")
	state_machine.initial_state = "State"
	add_child(state_machine)

func test_enter_state_on_ready():
	asserts.was_called(state, "enter")

func test_call_unhandled_input():
	state_machine._unhandled_input(null)
	asserts.was_called_with_arguments(state, "unhandled_input", [null])

func test_call_processes():
	simulate(state_machine, 1, 1)
	asserts.was_called(state, "process")
	asserts.was_called(state, "physics_process")

func test_transition_state():
	var new_state = Mockito.mock(State)
	new_state.method("enter")
	
	var new_state_double = new_state.double()
	state_machine.transition_to(new_state_double)
	
	asserts.was_called(state, "exit")
	asserts.was_called(new_state, "enter")
	asserts.is_equal(state_machine.state, new_state_double)
