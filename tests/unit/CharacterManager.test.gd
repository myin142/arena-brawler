extends WAT.Test

var character_manager: CharacterManager
var health: Mockito.ScriptDirector
var stats: Mockito.ScriptDirector

func pre():
	character_manager = CharacterManager.new()
	health = Mockito.mock(Health)
	stats = Mockito.mock(CharacterStats)

func _add_children() -> void:
	var root = Mockito.add_children(character_manager, {
		"Health": health,
		"CharacterStats": stats
	})
	add_child(root)

func test_initialize_with_max_health_from_stats():
	stats.method("get_max_health").stub(5)
	_add_children()

	asserts.is_equal(character_manager.health.max_health, 5)

func test_receive_damage():
	_add_children()
	character_manager.health.health = 5

	character_manager.damage(2)
	asserts.is_equal(character_manager.health.health, 3)
