extends WAT.Test

var health: Health
var max_health: int

func pre():
	health = Health.new()
	max_health = health.max_health
	watch(health, "changed")
	watch(health, "max_changed")
	add_child(health)

func test_initialize_with_max_health():
	asserts.is_equal(health.health, max_health)

func test_set_health():
	health.health = max_health - 2
	asserts.is_equal(health.health, max_health - 2)

func test_set_health_below_zero():
	health.health = -1
	asserts.is_equal(health.health, 0)
	
func test_set_health_above_max_health():
	health.health = max_health * 2
	asserts.is_equal(health.health, max_health)

func test_emit_on_health_change():
	health.health = max_health - 2
	asserts.signal_was_emitted_with_arguments(health, "changed", [max_health - 2])

func test_emit_on_zero_health():
	watch(health, "zero_health")
	health.health = 0
	asserts.signal_was_emitted(health, "zero_health")

func test_set_max_health():
	health.max_health = 20
	asserts.is_equal(health.max_health, 20)
	
func test_set_max_health_below_one():
	health.max_health = 0
	asserts.is_equal(health.max_health, 1)

func test_emit_on_max_health_changed():
	health.max_health = 5
	asserts.signal_was_emitted_with_arguments(health, "max_changed", [5])
	
# Currently only increase should be enough, there should be no use case to decrease max hp
func test_adjust_health_if_max_health_increases():
	health.max_health = max_health + 5
	asserts.is_equal(health.health, max_health + 5)
	asserts.signal_was_emitted_with_arguments(health, "changed", [max_health + 5])
