extends WAT.Test

var bullet: Bullet

func pre():
	bullet = Bullet.new()
	add_child(bullet)

func test_move_forward():
	simulate(bullet, 1, 1)
	asserts.is_greater_than(bullet.global_transform.origin.x, 0)

# Dont know how to test
#func test_destroy_after_two_seconds():
#	simulate(bullet, 2, 1)
#	asserts.is_freed(bullet)
