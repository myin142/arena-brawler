extends Spatial

export var size: int = 40

onready var texture: TextureRect = $TextureRect
onready var mesh: MeshInstance = $MeshInstance

var tmpMesh = Mesh.new()
var vertices = PoolVector3Array()
var UVs = PoolVector2Array()
var normals = PoolVector3Array()
var noise = OpenSimplexNoise.new()

func _ready():
	noise.seed = randi()
	noise.octaves = 9
	noise.period = 10
	noise.persistence = 0
	
	var image = noise.get_image(600, 400)
	var imgTex = ImageTexture.new()
	imgTex.create_from_image(image)
	texture.texture = imgTex
	
	if size < 0:
		size = 0
	
	for x in range(0, size):
		for y in range(0, size):
			_push_quad(x, y)

	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)

	for i in vertices.size():
		st.add_color(Color(1, 1, 1))
		st.add_uv(UVs[i])
		st.add_normal(normals[i])
		st.add_vertex(vertices[i])
	
	st.commit(tmpMesh)
	
	mesh.mesh = tmpMesh
	mesh.create_trimesh_collision()

func _push_quad(x: float, y: float) -> void:
	var vert1 = Vector3(x, noise.get_noise_2d(x, y), -y)
	var vert2 = Vector3(x, noise.get_noise_2d(x, y+1), -y-1)
	var vert3 = Vector3(x+1, noise.get_noise_2d(x+1, y), -y)
	var vert4 = Vector3(x+1, noise.get_noise_2d(x+1, y+1), -y-1)
	
	var uv1 = Vector2(vert1.x/10, -vert1.z/10)
	var uv2 = Vector2(vert2.x/10, -vert2.z/10)
	var uv3 = Vector2(vert3.x/10, -vert3.z/10)
	var uv4 = Vector2(vert4.x/10, -vert4.z/10)
	
	var side1 = vert2 - vert1
	var side2 = vert2 - vert4
	var normal1 = side1.cross(side2)
	
	vertices.push_back(vert1)
	vertices.push_back(vert2)
	vertices.push_back(vert4)
	
	UVs.push_back(uv1)
	UVs.push_back(uv2)
	UVs.push_back(uv4)
	
	for i in range(0, 3):
		normals.push_back(normal1)

	var side3 = vert4 - vert1
	var side4 = vert4 - vert3
	var normal2 = side3.cross(side4)

	vertices.push_back(vert1)
	vertices.push_back(vert4)
	vertices.push_back(vert3)

	UVs.push_back(uv1)
	UVs.push_back(uv4)
	UVs.push_back(uv3)
	
	for i in range(0, 3):
		normals.push_back(normal2)
